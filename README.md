# 草莓低代码开发平台thinkphp8+amis

#### 介绍
草莓低代码开发平台thinkphp8版本，前端是amis
![输入图片说明](https://foruda.gitee.com/images/1704355641466040978/0b4ca52e_1405153.png "屏幕截图")

#### 安装教程

PHP8，MySQL 8

1、git clone 克隆项目
2、composer install
3、导入项目根目录 database.sql
4、配置 .env 数据库链接信息
5、搭建网站，设置好thinkphp的伪静态
6、访问登录页面，账号：admin 密码：admin123

有问题欢迎提 issues



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
