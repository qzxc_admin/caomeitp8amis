<?php
namespace app\controller;
use app\model\CustomPage;
use app\Request;
use think\facade\Config;
use app\BaseController;
use app\model\AdminLog;
use think\facade\Db;
use think\facade\View;
use Tree;
use think\facade\Log;
use function MongoDB\BSON\toJSON;

/**
 * 自定义页面
 */
class Custom extends BaseController
{
    /**
     * @param $code
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 自定义页面引入amis定义的js
     */
    public function codeJs($code) {
        ob_end_clean();
        header('Content-Type: text/javascript;charset=UTF-8');
        $api_data = Db::table('custom_page')->where('code',$code)->find();
        try {
//            $json = json_decode($api_data['content']);
//            return json($json);
            return 'AMIS_JSON='.$api_data['content'];
        } catch (\Exception $e) {
            $msg = 'Api Error ['. $code .']: '. $e->getMessage();
            return $this->error($msg);
        }
    }

    /**
     * @param $code
     * @return string
     * 编辑器界面
     */
    public function editor($code) {
        View::assign('code', $code);
        View::assign('js', '/custom/codeJs?code='.$code);
        return View::fetch("amis/editor");
    }

    /**
     * @param $code
     * @return string
     * 预览页面
     */
    public function editorPage($code) {
        View::assign('code', $code);
        View::assign('js', '/custom/codeJs?code='.$code);
        return View::fetch("amis/page");
    }

    /**
     * @param $code
     * @return array|\think\response\Json
     * 预览界面数据回显
     */
    public function getCustomPage($code) {
        try {
            $api_data = Db::table('custom_page')->where('code', $code)->find();
            $json = json_decode($api_data['content']);
            return json([
                'schema' => $json,
                'code'=> $api_data['code']
            ]);
        } catch (\Exception $e) {
            $msg = 'Api Error ['. $code .']: '. $e->getMessage();
            return $this->error($msg);
        }
    }

    /**
     * @param Request $request
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 通过编辑器方式保存amis的数据
     */
    public function saveSchema(Request $request) {
        $code = $request->param("code");
        $schema = $request->param('schema');
        $where[] = ['code','=',$code];
        $customPage = CustomPage::where($where)->find();
        CustomPage::update([
            'id' => $customPage['id'],
            'code' => $code,
            'name'=> $customPage['name'],
            'content'=> $schema
        ]);
        return $this->success('保存成功', $customPage);
    }

    /**
     * @param Request $request
     * @return \think\response\Json
     * @throws \think\db\exception\DbException
     * 通过代码方式保存amis的数据
     */
    public function updateSchema(Request $request) {

        // 获取主键字段名
        $code = $request->post("code");     // 获取 id
        $data = $request->post();           // 获取请求数据
        // 如果属性中存在数组则使用 JSON 编码
        foreach($data as &$val)
        {
            if(is_array($val)) {
                $val = json_encode($val,JSON_UNESCAPED_UNICODE);
            }
        }
        // 如果id不为空 则表示修改数据, 如果不存在就表示新增数据
        if (!empty($code)){
            Db::table("custom_page")->where(['code' => $code])->update([
                'content'=> $data['schema']
            ]);
        }

        return $this->success('保存成功');
    }
}














