<?php
namespace app\controller;
use app\model\Config;
use app\Request;
use app\BaseController;
use app\model\AdminLog;
use think\facade\Db;
use think\facade\View;
use Tree;
use think\facade\Log;
use function MongoDB\BSON\toJSON;

// 通用上传接口
class CustomConfig extends BaseController
{

    public function get($code) {
        $api_data = Db::table('config')->where('code',$code)->find();
        try {
            $json = json_decode($api_data['value']);
            return json($json);
        } catch (\Exception $e) {
            $msg = 'Api Error ['. $code .']: '. $e->getMessage();
            return $this->error($msg);
        }
    }

    public function save(Request $request) {
        $code = $request->param("code");
        $value = $request->getContent();
        $where[] = ['code','=',$code];
        $config = Config::where($where)->find();
        if (empty($config['id'])) {
            Config::create([
                'code' => $code,
                'name' => $code,
                'value' => $value
            ]);
        } else {
            Config::update([
                'id' => $config['id'],
                'code' => $code,
                'name' => $config['name'],
                'value' => $value
            ]);
        }
        return $this->success('保存成功');
    }
}














