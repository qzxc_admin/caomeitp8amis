<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class CustomPage extends Model
{
    protected $schema = [
        'id' => 'int',
        'code' => 'string',
        'name' => 'string',
        'content' => 'string'
    ];

}
