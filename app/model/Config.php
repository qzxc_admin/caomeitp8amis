<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class Config extends Model
{
    protected $schema = [
        'id' => 'int',
        'code' => 'string',
        'name' => 'string',
        'value' => 'string'
    ];

}
