AMIS_JSON={
  "data": {},
  "asideResizor": false,
  "type": "page",
  "title": "网站配置",
  "body": [
    {
      "type": "form",
      "title": "",
      "body": [
        {
          "type": "fieldSet",
          "title": "网站配置",
          "body": [
            {
              "label": "标题",
              "type": "input-text",
              "name": "title",
              "id": "u:20eed28af140"
            },
            {
              "label": "简介",
              "type": "input-rich-text",
              "name": "briefIntroduction",
              "id": "u:be770f184455"
            }
          ],
          "mode": "horizontal",
          "collapsable": false,
          "id": "u:5508ae70b720"
        },
        {
          "type": "fieldSet",
          "title": "Banner配置",
          "body": [
            {
              "editable": true,
              "columns": [
                {
                  "label": "标题",
                  "type": "input-text",
                  "name": "title",
                  "width": 100
                },
                {
                  "label": "链接",
                  "type": "input-text",
                  "name": "url",
                  "width": 100
                }
              ],
              "label": "Banner",
              "type": "input-table",
              "addable": true,
              "draggable": true,
              "removable": true,
              "needConfirm": false,
              "name": "banners",
              "id": "u:8b16530247ca"
            }
          ],
          "mode": "horizontal",
          "collapsable": false,
          "id": "u:2d8d1438b185"
        }
      ],
      "mode": "horizontal",
      "submitText": "保存",
      "api": {
        "method": "post",
        "dataType": "json",
        "url": "/admin/config/save/webSite",
        "messages": {}
      },
      "wrapWithPanel": true,
      "id": "u:95b6a4e70964",
      "actions": [
        {
          "type": "submit",
          "label": "保存",
          "primary": true
        }
      ]
    }
  ],
  "pullRefresh": {
    "disabled": true
  },
  "style": {
    "boxShadow": " 0px 0px 0px 0px transparent"
  },
  "id": "u:0840c7d691c2"
}


