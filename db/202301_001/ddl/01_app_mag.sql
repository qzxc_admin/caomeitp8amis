CREATE TABLE IF NOT EXISTS `app_mag`
(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(64) NOT NULL comment '应用名称',
  `app_icon` varchar(255) DEFAULT NULL,
  `create_time` datetime NOT NULL comment '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB comment '应用管理';
